'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Avenida', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type:Sequelize.INTEGER,
      },
      nome: {
        allowNull: false,
        type:Sequelize.STRING,
        unique: true
      },
      extensao: {
        allowNull: false,
        type:Sequelize.INTEGER,
      },
      ciclofaixa: {
        allowNull: false,
        type:Sequelize.BOOLEAN,
        defaultValue: false
      },
      extensaoCiclofaixa: {
        type:Sequelize.INTEGER,
        defaultValue: 0
      },
      percentagemCiclofaixa: {
        type:Sequelize.DOUBLE,
        defaultValue: 0
      },
      createdAt: {
        allowNull: false,
        type:Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type:Sequelize.DATE,
      },
    });
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.dropTable('Avenida');
  }
};
