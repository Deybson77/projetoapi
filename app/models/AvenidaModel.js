module.exports = (sequelize, DataTypes) => {
	const Avenida = sequelize.define('Avenida', {
		nome: DataTypes.STRING,
		extensao: DataTypes.INTEGER,
		ciclofaixa: DataTypes.BOOLEAN,
		extensaoCiclofaixa: DataTypes.INTEGER,
		percentagemCiclofaixa: DataTypes.DOUBLE
	}, {freezeTableName: true})
	return Avenida
}