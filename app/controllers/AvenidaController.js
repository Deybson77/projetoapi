const {Avenida} = require('../models')

exports.salvarAvenida = async (req, res) => {
	let {ciclofaixa} = req.body
	if(ciclofaixa == true || ciclofaixa == 1){
		let {extensao, extensaoCiclofaixa} = req.body
		let percentagem = await(extensaoCiclofaixa*100)/extensao
		req.body.percentagemCiclofaixa = percentagem
	}
	let avenida=null
	try{
		avenida = await Avenida.create(req.body)
		return res.status("201").json({msg: 'Avenida Cadastrada',
								       avenida})
	}catch(err){
		return res.status("400").json({err})
	}
}

exports.listarAvenidas = async (req, res) => {
	let avenida=null
	try{
		avenida = await Avenida.findAll()
		return res.status("200").json({avenida})
	}catch(err){
		return res.status("400").json({err})
	}
}
exports.consultarAvenida =  async (req, res) => {
	let avenida=null
	let teste = await req.params.nome
	await console.log(teste)
	try{

		avenida = await Avenida.findAll({where : {nome :req.params.nome}})
		return res.status("200").json({avenida})
	}catch(err){
		return res.status("400").json({err})
	}
}
exports.excluirAvenida =  async (req, res) => {
	
	try{

		await Avenida.destroy({where : {id :req.params.nome}})
		return res.status("200").json({msg : "excluido com sucesso"})
	}catch(err){
		return res.status("400").json({err})
	}
}