module.exports = (app) =>{
	const avenidaController = require("../controllers/AvenidaController")



	app.route('/avenidas')
		.get(avenidaController.listarAvenidas)
		.post(avenidaController.salvarAvenida)

	app.route('/avenidas/:nome')
		.get(avenidaController.consultarAvenida)
		.delete(avenidaController.excluirAvenida)
}
