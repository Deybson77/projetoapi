const express = require('express'),
	  bodyParser = require('body-parser'),
	  port = process.env.PORT || 3000,
	  app = express(),
	  cors = require("cors")


app.use(cors())

app.use(bodyParser.json({extended: true}))
app.use(bodyParser.urlencoded({extended : true}))

const route = require('./app/routes/routes')

route(app)

app.listen(port)
console.log("rodando na porta 3000")